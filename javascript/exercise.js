/* exercise.js */

var Exercise = function() {
  return this;
};

/*
  * Implement a function that, when given an array of integers,
  * returns the array with all odd numbers removed
  */
Exercise.prototype.remove_odds_from_array = function(arr) {
  for (var ii = 0; ii < arr.length; ii++) { // loop through each element of given array
  	if (arr[ii] % 2 != 0) { // if array element at current index is odd:
  	  arr.splice (ii, 1); // remove element from array
      ii--; // subtract one from current index to account for removed array element
    }
  }
  return arr; // return array with odd elements removed
};

/*
 * Implement a function that converts between fahrenheit and celsius
 *
 * Function arguments:
 *   value: an integer temperature value to convert from
 *   type: a character indicating the format (fahrenheit or celsius) of argument "value"
 *
 * For argument "type", the function should be able to accept values of "C" and "F".
 *
 * The return value should be a string containing the converted value, rounded to 2 decimal places,
 * along with a space and the letter type of the converted value (eg. "80.23 F")
 */
Exercise.prototype.convert_fahrenheit_and_celsius = function(value, type) {
    switch (type) {
    case 'F': // convert from Celsius to Fahrenheit
    	value = ((value - 32) * 5) / 9; // conversion formula
    	sValue = value.toFixed(2) + " C"; // round to two decimal places and add converted value type, as per spec
    	break;
    case 'C': // convert from Fahrenheit to Celsius
    	value = ((value * 9) / 5) + 32; // conversion formula
    	sValue = value.toFixed(2) + " F"; // round to two decimal places and add converted value type, as per spec
    	break;
    default:
    	sValue = false; // return false if no 'type' is given
    	break;	  
  }
  return sValue; // return converted value
};

# exercise.rb

class Exercise

  # Implement this method such that it returns the sum of
  # an array of integers
  def self.sum_array(arr)
    total = 0 # start total value at zero
    for num in arr # loop through each array element
      total += num # add current array element to total value
    end
    return total # return total value
  end

  # For processing credit card payments, Paypal adds a fee
  # of 2.9% of the charged value, plus an additional $0.30 
  # flat fee in addition to that.
  #
  # Define the following method so that it takes the subtotal,
  # and returns a total with these fees applied.
  #
  # Round up to the nearest cent.
  # 
  def self.paypal_credit_card_fee(subtotal)
    total = (subtotal * 1.029) + 0.30 # add 2.9% fee and $0.30 flat fee to subtotal
    total = (total * 100.00).round / 100.00 # round total up to nearest cent
    return total # return total value
  end

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
   arr = str.split(" ") # create array of individual words by splitting original string on ' '
    for ii in 0..arr.length - 1 # loop through each word in array (loop by index so that array value can be easily replaced)
      if (arr[ii].length > 4) # if word is more than four characters long
        if (arr[ii][0,1].upcase == arr[ii][0,1]) # check to see if first character is uppercase
          arr[ii] = "Marklar" # if so, replace word with 'Marklar'
        else
          arr[ii] = "marklar" # otherwise, replace word with 'marklar'
        end
      end
    end
    return arr.join(" ") # join array elements back together, separated by spaces, and return that value
  end
end
